# Book Link Weight

Replaces the book outline weight element with a drag & drop table element, allowing users to position book pages more easily.

# Installation

Enable the module like you would any other drupal.org module.

# Configuration

After enabling the module all node edit / add forms should now include the new drag & drop interface for positioning a node within a book.