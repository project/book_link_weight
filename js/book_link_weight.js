/**
 * @file
 * Book Link Weight Javascript functionality.
 *
 * @see book_link_weight.js
 */

(function ($) {
  /**
   * Automatically update the current link title in the book link weight list.
   */
  Drupal.behaviors.bookLinkWeightAutomaticTitle = {
    attach: function (context) {
      var $context = $(context);
      $context.find('#edit-book-weight-wrapper').each(function () {
        var $this = $(this);
        var $current_selection = $this.find('.book-item-weight-current-item');
        var $title = $this.closest('form').find('.js-form-item-title-0-value input');

        // Also update on node title change, as this may update the link title.
        $title.on('keyup', function () {
          $current_selection.html($title.val());
        });
      });
      /*
       * TLDR; In order to make book display the tree of nodes for the
       * purpose of re-ordering we need the ajax event from the second
       * dependant dropdown to be fired after the book ID (bid) has been
       * changed (which in itself populates the parent ID (pid) from an
       * AJAX call). We are entering a slightly inception-y state, hence the
       * check for the triggering element.
       *
       * Due to the book module being a closed somewhat antiquated book, there
       * is very little scope for alteration and extension.
       *
       * As a result of this we need to be able to make use of the AJAX part of
       * its process, which causes a dependant drop-down to be populated by a
       * list of nodes. As part of this the parent ID drop-down is already
       * selected with the first node.
       *
       * In order to present the CMS user with a dragdrop tree of nodes, those
       * nodes must be presented after the parent ID has been selected, which
       * happens immediately on load.
       *
       * So as soon as the ajax request for the parent ID list is complete
       * (after a book ID has been selected) we then trigger a change event
       * on the parent ID selector, which in turn fires an AJAX call to
       * return the sibling elements to be re-ordered.
       *
       * In an ideal world we would either prevent a default parent ID being
       * selected, or have the first AJAX call return both fields, due to the
       * age and inflexibility of the book module, neither of these options are
       * available.
       */
      $context.ajaxComplete(function (event, jqXHR, ajaxOptions) {
        if (ajaxOptions.extraData && ajaxOptions.extraData._triggering_element_name == 'book[bid]') {
          $('.js-book-parent-select').trigger('change');
        }
      });
    }
  };

})(jQuery);